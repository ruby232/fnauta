# Get Captcha

GET /captcha/captcha HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Encoding: gzip, deflate, br
Accept-Language: es,en-US;q=0.9,en;q=0.8,it;q=0.7,ca;q=0.6,pt;q=0.5
Connection: keep-alive
Host: www.nauta.cu:5002
If-None-Match: W/"4135-/VzQ7Qs++zchWYQRtQwtDNtPhJ4"
Origin: https://www.nauta.cu
Referer: https://www.nauta.cu/
Sec-Fetch-Dest: empty
Sec-Fetch-Mode: cors
Sec-Fetch-Site: same-site
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36
sec-ch-ua: "Google Chrome";v="105", "Not)A;Brand";v="8", "Chromium";v="105"
sec-ch-ua-mobile: ?0
sec-ch-ua-platform: "Linux"

## Response

```json
 {
  "text": "41d3a8ba8dee031b0f3ef93db6a1117b080fc5ed",
  "data": "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"250\" height=\"60\" viewBox=\"0,0,250,60\"><rect width=\"100%\" height=\"100%\" fill=\"#cc9966\"/><path d=\"M1 14 C108 29,119 45,241 17\" stroke=\"#86d0e8\" fill=\"none\"/><path d=\"M16 36 C129 23,107 13,241 5\" stroke=\"#45ddb7\" fill=\"none\"/><path d=\"M3 55 C119 39,143 44,245 43\" stroke=\"#a7e692\" fill=\"none\"/><path fill=\"#46484d\" d=\"M215.21 14.51C214.76 14.46 214.42 14.48 214.26 14.56C211.17 15.23 209.67 15.73 209.67 16.03L197.74 42.74C197.22 45.12 196.07 46.18 194.72 46.27L194.00 46.21C193.92 46.23 193.82 46.23 193.82 46.38L193.79 49.74C193.73 49.78 193.82 49.88 193.93 49.83L209.24 49.90L209.38 49.73C209.36 49.66 208.37 49.18 206.21 48.06C204.80 47.60 204.02 46.98 204.05 46.25C203.94 45.95 204.12 45.68 204.24 45.10C204.47 44.38 205.12 44.08 206.12 44.03L210.78 44.03C212.00 44.06 212.62 44.43 212.60 45.21L212.63 45.58C212.11 46.32 211.92 46.83 211.86 46.87L211.95 47.30C211.83 47.74 212.32 48.18 213.03 48.29C213.84 48.10 214.27 47.78 214.19 47.20L214.31 46.86C213.87 46.17 213.62 45.63 213.67 45.37L213.57 45.17C213.68 44.48 214.22 44.08 215.43 44.13L221.82 44.02C222.07 44.57 222.17 44.88 222.17 45.07C222.04 45.50 222.02 45.83 221.79 45.75C221.36 46.42 220.12 47.63 218.51 49.82L218.38 49.83L231.73 49.93L231.79 49.69C231.72 48.23 231.52 46.98 231.42 46.28C230.48 46.13 229.87 45.98 229.58 45.83C229.19 45.40 228.42 45.03 227.19 44.55C226.21 44.06 225.72 42.08 225.61 38.31C225.73 38.29 222.87 31.68 217.23 18.79L217.09 18.54L217.35 18.55C218.45 18.30 218.92 17.68 219.00 16.95L218.90 16.45C217.74 15.10 216.57 14.43 215.49 14.45ZM200.63 45.68C200.56 45.02 202.52 40.68 206.31 32.37C209.50 25.01 211.27 21.23 211.45 20.86C211.74 21.59 211.87 21.93 211.89 22.14L211.88 22.38C211.57 23.32 207.97 31.23 201.27 46.22C200.75 46.05 200.62 45.98 200.55 45.60ZM211.78 28.93C212.07 27.62 212.52 27.03 213.22 27.02L213.26 27.02C214.18 27.08 214.67 27.68 214.93 29.08L214.83 29.08C214.78 29.88 214.37 30.23 213.93 30.28L212.78 30.24C212.25 30.25 211.92 29.88 211.80 29.20ZM209.99 32.65C210.19 31.75 210.62 31.33 211.24 31.30L212.63 31.34L212.52 32.58C212.37 33.48 212.12 34.03 211.32 33.93C210.56 33.96 210.07 33.58 210.11 32.96ZM213.72 32.67C213.76 31.77 214.12 31.33 214.76 31.37L215.36 31.32C215.96 31.31 216.37 31.78 216.56 32.71C216.52 33.67 215.97 34.03 215.32 34.12L214.97 34.03C214.06 33.91 213.62 33.53 213.61 32.71ZM208.16 37.12C208.14 37.00 208.17 36.58 208.54 36.05C208.70 35.35 209.17 35.03 210.00 35.05L212.63 35.04L212.53 35.58C212.63 36.58 212.07 37.03 211.03 37.03ZM213.64 35.04L217.57 35.02C217.68 34.99 217.82 35.28 217.73 35.59L217.91 36.06C217.78 36.63 217.27 37.03 216.13 36.98L215.17 36.98C214.27 37.12 213.62 36.68 213.72 35.97ZM205.92 41.62C206.14 40.95 206.67 40.68 207.49 40.65L212.61 40.66L212.59 41.60ZM213.58 40.64L220.06 40.71C220.04 40.59 220.32 40.98 220.54 41.44L220.65 41.66L213.62 41.63Z\"/><path fill=\"#283038\" d=\"M52.63 15.75L52.55 15.92C52.50 15.96 53.61 17.13 55.85 19.41C56.95 19.42 57.46 19.68 57.45 20.17L57.51 21.12C56.97 28.04 56.66 31.98 56.72 33.14C56.72 34.49 56.76 35.58 57.02 36.69C57.52 38.94 57.91 40.78 57.82 41.79L57.83 42.20C57.88 43.45 57.56 44.63 56.78 45.65C56.76 46.22 56.66 46.58 56.36 46.52C55.67 46.49 55.31 47.33 54.97 48.79L55.08 49.25C55.08 49.84 55.56 50.18 56.58 50.34C58.80 50.27 62.61 50.28 67.70 50.17C68.37 49.79 68.71 49.48 68.67 49.09C68.63 48.65 68.61 48.33 68.28 47.95C67.39 47.61 66.96 46.93 66.94 45.81C67.01 44.98 66.96 44.13 67.06 43.43C66.91 42.43 67.71 42.13 68.96 42.03L69.01 42.03L69.17 42.58C69.07 42.69 69.11 42.98 69.02 43.24C68.39 43.95 68.11 44.58 68.09 44.95C68.04 45.66 68.61 46.23 69.59 46.36C70.51 46.48 70.91 45.93 71.11 45.03L71.00 44.76C71.13 44.34 70.76 43.83 70.23 43.54L70.18 43.40C70.23 42.69 70.61 42.18 71.58 42.19L74.66 42.08C75.35 42.22 75.61 42.23 76.00 42.57C77.57 45.19 78.36 46.68 78.42 47.14L78.30 47.22C78.46 47.82 77.86 48.68 77.01 50.12C76.97 50.18 76.91 50.18 77.07 50.38C83.45 50.17 87.36 50.23 88.50 50.17C89.05 50.17 89.51 49.78 89.65 48.92C89.14 47.36 88.76 46.68 88.29 46.61C86.84 46.66 85.81 46.53 85.24 46.21C84.64 45.51 82.36 41.53 78.09 34.01C75.85 30.22 74.56 27.93 74.65 27.62L74.57 27.19C74.50 26.76 76.51 24.78 80.40 21.01C80.96 19.88 82.61 19.43 85.01 19.33C85.34 19.35 86.56 18.23 88.59 15.85C88.57 15.73 88.56 15.78 88.37 15.68L76.73 15.80C75.75 15.72 75.36 16.33 75.30 17.32L75.44 19.31C75.31 19.38 75.76 19.48 76.51 19.43C75.94 20.66 75.16 21.08 74.54 21.16L68.32 21.09C67.47 21.09 67.01 20.58 67.02 19.59L66.94 19.26C67.09 19.26 67.11 18.93 67.34 18.71C68.06 18.17 68.36 17.63 68.41 17.17L68.31 16.93C68.35 16.22 67.86 15.83 66.75 15.77ZM63.58 21.19C63.53 20.20 63.86 19.68 64.48 19.45L64.50 19.42L64.48 30.75C64.62 31.53 64.26 32.13 63.62 32.78L63.37 32.64L63.47 31.09C63.45 30.47 63.46 29.98 63.40 29.62C63.41 29.07 63.46 28.78 63.41 28.47C63.46 27.48 63.51 25.03 63.56 21.18ZM67.07 26.39L66.95 23.32C67.04 22.56 67.61 22.13 68.74 22.16L71.09 22.21C71.92 22.24 72.36 22.63 72.37 23.29C72.32 23.74 72.16 24.08 71.82 24.24C69.88 26.35 68.76 27.43 68.33 27.40L68.02 27.34C67.51 27.38 67.21 27.08 66.96 26.28ZM66.95 34.87C67.05 34.32 67.71 33.48 69.10 32.62L69.12 32.58L69.20 36.46L68.59 36.36C67.52 36.33 67.01 35.98 66.97 35.08ZM70.17 32.73L70.30 32.82C71.09 34.00 71.51 34.78 71.49 35.10C71.50 35.77 71.06 36.23 70.15 36.37ZM63.48 39.00C63.51 38.07 63.86 37.33 64.61 36.92L64.46 46.43L64.08 46.35C63.74 46.45 63.46 45.98 63.49 45.10ZM70.20 38.72C70.28 37.75 70.71 37.33 71.38 37.30C71.33 37.30 71.46 37.38 71.43 37.30L72.12 37.34C73.08 37.40 73.76 38.23 74.23 39.85L74.29 40.01C74.28 40.84 73.51 41.18 72.23 41.24C70.87 41.23 70.16 40.73 70.22 39.88ZM67.06 40.13L66.93 38.50C66.95 37.72 67.61 37.38 68.75 37.32L69.08 37.35L69.14 41.15L68.71 41.03C67.68 41.19 67.01 40.78 67.08 40.14Z\"/><path d=\"M4 5 C142 54,128 52,229 17\" stroke=\"#9852df\" fill=\"none\"/><path fill=\"#373837\" d=\"M168.45 15.76C166.72 15.73 163.92 15.73 160.22 15.83L160.20 15.96C160.75 17.01 161.17 17.68 161.55 18.16C162.73 18.34 163.32 18.88 163.33 19.69C163.40 19.76 163.27 19.78 163.30 20.06C163.20 21.20 163.17 22.78 163.05 24.70C162.21 30.62 161.77 34.28 161.76 35.77L161.72 36.08C161.86 37.47 161.97 38.68 162.46 40.12C163.66 42.41 164.17 44.13 164.26 45.51L164.21 45.76C164.09 46.45 163.97 47.53 163.54 48.80L163.65 49.01C163.56 49.17 163.72 49.48 163.81 49.67C163.83 50.09 164.92 50.33 166.98 50.29L175.43 50.29C180.49 50.25 184.57 49.18 187.44 46.85C192.28 43.14 194.62 38.63 194.63 33.44L194.70 32.65C194.70 29.06 193.32 25.48 190.80 22.21C188.37 19.23 185.47 17.38 181.62 16.18C180.12 15.78 176.02 15.68 169.02 15.58ZM169.17 45.88L169.31 20.07C169.26 18.92 169.67 18.33 170.71 18.37L170.90 18.25C171.72 18.33 172.07 18.83 172.07 19.93L172.16 35.26C172.08 38.09 172.12 41.88 172.18 46.59C172.23 47.23 171.77 47.53 170.98 47.73L170.61 47.62C169.73 47.74 169.22 47.08 169.28 45.99ZM174.55 19.66C174.40 18.76 174.87 18.43 175.65 18.26L176.17 18.28C179.52 18.32 181.22 18.83 181.22 19.92C181.20 20.81 180.67 21.28 179.60 21.26L175.87 21.33C174.87 21.22 174.47 20.68 174.42 19.52ZM174.51 23.87L174.54 23.14C174.67 22.63 175.32 22.33 176.47 22.18C176.44 22.20 176.52 22.28 176.54 22.25L177.78 22.24C178.44 22.25 178.82 22.68 178.89 23.45L179.01 23.86C178.94 24.60 178.22 24.98 176.84 25.00C175.25 24.96 174.52 24.63 174.50 23.86ZM179.94 23.55C179.86 22.77 180.37 22.43 181.21 22.22L184.29 22.25C185.23 22.43 185.67 22.83 185.68 23.48L185.62 23.68C185.50 24.40 185.22 24.88 184.50 24.80L181.15 24.91C180.22 24.63 179.92 24.28 179.82 23.43ZM174.59 39.90L174.47 26.63C174.72 26.13 175.47 25.93 176.57 25.88L177.76 25.87C178.32 25.87 178.72 26.28 178.87 26.97L178.93 39.84C178.86 40.42 178.52 40.83 177.61 40.87L177.25 41.01C175.41 40.92 174.52 40.58 174.51 39.82ZM179.86 39.01L179.83 27.59C179.90 26.45 180.47 25.93 181.50 25.90L185.77 25.87C186.21 26.02 186.37 25.93 186.76 26.07C187.65 26.01 188.22 28.18 188.60 32.61L188.64 33.65C188.12 38.33 187.32 40.78 186.17 40.83L181.05 40.85C180.35 40.86 179.92 40.28 179.95 39.11ZM174.42 44.67C174.43 43.29 174.52 42.63 174.68 42.59C174.75 42.16 175.37 41.88 176.75 41.91C177.85 41.91 178.32 42.28 178.35 43.16C178.42 43.37 178.27 43.63 178.17 44.22L178.16 44.57C178.28 45.39 178.57 45.73 179.23 45.79L179.62 45.68C180.24 45.64 180.72 45.28 180.79 44.29L180.90 44.35C180.81 44.12 180.77 43.83 180.56 43.22L180.61 43.12C180.66 42.37 181.02 41.93 181.81 41.97L183.94 41.90C184.66 42.01 184.92 42.33 185.16 43.21C185.11 43.67 184.77 44.13 184.11 44.67C182.36 46.62 179.87 47.68 176.41 47.62L175.65 47.71C175.44 47.60 175.37 47.68 175.19 47.55C174.62 47.53 174.47 46.78 174.37 44.98Z\"/><path fill=\"#463c39\" d=\"M95.05 15.74C95.07 15.86 94.94 15.83 95.02 16.01L95.01 36.20L95.67 42.86C95.63 43.82 95.34 44.68 95.08 45.62C94.75 46.09 94.19 46.53 93.00 46.44C92.80 46.44 91.59 47.73 89.10 50.04L89.14 50.28L113.44 50.28L113.58 50.12C109.97 49.06 107.54 48.23 106.47 47.86C105.47 47.76 104.89 47.18 104.97 46.41L104.96 39.14C105.06 38.30 105.39 37.83 106.16 37.90L110.74 37.83C114.26 37.75 116.79 37.33 118.06 36.30C122.03 33.97 123.89 30.73 123.93 26.77C123.96 24.30 123.04 21.88 121.36 19.80C119.14 16.98 116.19 15.78 111.99 15.68ZM100.99 17.03C102.02 17.06 102.44 17.48 102.47 18.36L102.38 21.22C102.37 21.96 101.99 22.43 101.02 22.46L100.97 22.51ZM104.97 19.26C104.83 18.32 105.34 17.98 106.08 17.82L111.98 17.97C112.81 17.95 113.59 18.08 114.51 18.45C115.43 18.97 116.09 19.83 116.58 21.12L116.54 21.43C116.51 22.15 116.04 22.53 115.01 22.50L110.20 22.54L110.11 21.85C110.18 21.82 110.24 21.68 110.28 21.57C110.75 20.99 111.04 20.53 111.00 20.14C111.04 19.43 110.59 18.98 109.69 18.83C109.00 18.89 108.49 19.28 108.40 20.24L108.42 20.61C108.96 21.20 109.19 21.83 109.21 22.45L109.26 22.60L106.29 22.58C105.36 22.45 104.89 22.03 104.91 21.35ZM100.91 23.45L101.26 23.60C101.97 23.56 102.39 23.98 102.47 24.86L102.37 25.31C102.33 26.22 101.94 26.68 101.23 26.67L101.01 26.70ZM104.92 24.86C104.97 24.11 105.34 23.63 106.22 23.61L108.09 23.58C108.82 23.56 109.19 24.03 109.22 25.11C109.24 26.18 108.84 26.68 108.14 26.73L105.95 26.64C105.38 26.67 104.99 26.23 104.88 25.37ZM110.28 24.72C110.26 24.10 110.64 23.68 111.56 23.60L116.10 23.49C117.22 23.55 117.74 24.18 117.77 25.50C117.83 26.27 117.34 26.58 116.63 26.77L111.28 26.72C110.54 26.58 110.34 26.28 110.09 25.38ZM100.93 27.72L101.33 27.87C102.03 27.87 102.34 28.18 102.53 29.12L102.37 29.66C102.42 30.46 101.99 30.88 101.07 30.96L101.07 31.06ZM104.86 29.00C105.03 28.22 105.39 27.78 106.03 27.82L108.01 27.70C108.88 27.82 109.19 28.28 109.23 29.32C109.23 30.47 108.64 30.98 107.63 31.02L106.31 31.05C105.38 30.92 104.89 30.48 104.93 29.82ZM110.23 28.97C110.25 28.24 110.59 27.78 111.45 27.84L116.49 27.73C117.05 27.74 117.44 28.18 117.60 28.89C117.60 30.24 117.04 30.98 115.85 30.94L111.66 30.90C110.64 30.88 110.19 30.63 110.09 29.78ZM100.89 31.88L101.22 32.01C101.96 32.00 102.39 32.43 102.46 33.25L102.37 48.91L102.09 48.98C101.51 48.94 101.19 48.58 100.96 47.84L100.95 47.14C101.01 46.45 100.99 46.08 100.96 46.10C101.08 45.56 100.99 44.93 101.08 44.56ZM104.96 33.30C104.91 32.49 105.34 32.08 106.16 31.99L109.14 31.93C109.20 32.14 109.19 32.33 109.10 32.69C108.55 33.44 108.34 33.98 108.30 34.04L108.37 34.21C108.32 34.91 108.74 35.38 109.52 35.51L109.56 35.65L106.15 35.69C105.32 35.56 104.99 35.18 104.82 34.26ZM110.10 31.89L114.60 31.89C115.42 31.96 115.89 32.43 115.92 33.21L115.99 33.63C115.10 34.93 113.59 35.68 111.20 35.63L110.00 35.74C109.82 35.66 109.79 35.63 109.77 35.51C110.42 35.51 110.84 35.13 111.02 34.31C111.05 34.19 111.04 33.98 111.00 33.79C110.42 33.16 110.19 32.68 110.17 32.31Z\"/><path d=\"M19 28 C130 30,138 39,234 51\" stroke=\"#bc91e7\" fill=\"none\"/><path fill=\"#262135\" d=\"M15.39 15.82L15.20 15.87C16.16 17.48 16.80 18.53 17.01 18.78C16.98 19.25 17.65 19.58 18.83 19.50C19.56 19.99 20.00 21.18 19.91 22.69C19.58 24.90 19.40 26.33 19.38 26.90L19.41 32.33C19.32 36.64 20.35 40.33 22.22 43.04C25.67 47.89 29.70 50.33 34.32 50.29C37.09 50.42 39.65 49.53 42.29 48.02C47.11 44.09 49.65 39.18 49.61 33.04L49.64 21.91C49.75 20.52 49.85 19.58 50.35 19.52L52.86 19.43L56.13 15.91C56.13 15.80 56.10 15.78 55.93 15.75L40.45 15.72L40.35 15.87C42.16 18.28 43.00 19.43 43.11 19.48L44.31 19.48C44.54 19.36 44.95 19.43 45.29 19.41C45.87 19.54 46.00 20.43 46.07 22.34L45.98 23.21C45.96 24.33 45.65 24.98 44.86 24.93L38.18 24.90C37.79 25.06 37.35 24.63 37.34 24.06L37.31 22.58C37.25 22.37 37.30 22.23 37.40 22.02C38.33 21.40 38.65 20.68 38.73 20.15C38.45 18.67 37.85 17.98 36.70 17.97C35.41 18.28 34.70 18.88 34.76 19.88L34.68 20.06C34.62 20.24 34.80 20.63 34.87 20.94C35.70 21.67 36.15 22.23 36.10 22.47L36.13 23.95C36.11 24.43 35.80 24.83 35.06 24.93L31.40 25.07C30.66 25.03 30.20 24.48 30.26 23.53L30.15 18.32C30.12 18.09 30.30 17.83 30.42 17.39C32.07 16.49 32.85 15.93 32.87 15.89L32.92 15.84ZM25.48 19.70C25.63 19.75 25.65 19.73 25.68 19.70C26.98 19.50 27.60 19.33 27.73 19.20L27.80 19.22L27.75 40.32L27.65 40.27C26.32 38.24 25.50 35.63 25.57 32.74ZM30.12 27.49C30.19 26.57 30.60 26.08 31.29 26.07L35.01 25.98C35.64 26.06 36.00 26.43 36.14 27.06L36.17 29.39C36.23 30.05 35.70 30.38 34.93 30.55L31.57 30.54C30.70 30.37 30.20 29.88 30.25 29.17ZM37.34 27.17C37.25 26.52 37.55 26.23 38.25 26.07L44.97 26.14C45.61 26.03 46.00 26.68 45.96 27.78L46.08 28.70C46.02 29.84 45.55 30.48 44.67 30.49L38.45 30.42C37.63 30.35 37.25 29.98 37.23 29.35ZM30.23 32.85C30.41 31.98 30.75 31.48 31.46 31.53L34.77 31.39C35.63 31.50 36.15 31.98 36.08 32.50L36.19 34.36C36.06 34.98 35.55 35.43 34.26 35.33L32.10 35.52C30.73 35.35 30.20 34.93 30.13 33.75ZM37.17 32.59C37.27 31.99 37.65 31.58 38.52 31.49L44.65 31.47C45.37 31.54 45.75 31.83 46.02 32.64C45.75 32.98 45.85 33.38 45.75 33.43C45.88 34.80 45.20 35.43 43.93 35.45L39.00 35.42C37.83 35.46 37.25 35.08 37.28 34.36ZM30.18 37.95C30.28 37.05 30.65 36.48 31.58 36.55L34.92 36.44C35.71 36.43 36.15 36.88 36.11 37.58L36.09 38.16C36.21 38.58 36.05 38.78 35.96 39.03C35.06 39.53 34.70 40.18 34.66 40.73L34.77 41.09C34.99 42.32 35.55 42.88 36.49 42.92L37.00 42.97C37.80 42.87 38.40 42.28 38.65 41.02L38.58 40.75C38.75 40.82 38.60 40.48 38.60 40.12C37.74 39.31 37.25 38.63 37.34 38.31L37.31 37.53C37.16 36.83 37.60 36.58 38.31 36.38L43.55 36.52C43.93 36.40 44.40 36.83 44.48 37.50C44.49 38.02 44.35 38.63 43.84 39.12C41.88 42.45 38.95 44.13 35.08 44.10L34.70 44.22C33.64 44.11 32.80 43.98 32.09 43.71C30.83 43.40 30.20 42.83 30.18 41.75Z\"/><path d=\"M4 46 C104 22,133 49,240 37\" stroke=\"#f0bc88\" fill=\"none\"/><path fill=\"#3b2f2f\" d=\"M140.77 15.73C137.19 15.75 133.96 16.78 131.04 18.70C125.78 22.35 123.26 27.33 123.18 33.25C123.19 37.20 123.96 40.18 125.29 41.95C129.06 44.97 131.36 46.63 132.56 47.37C135.16 49.32 138.36 50.33 142.31 50.37L143.06 50.27C143.32 50.23 143.91 50.28 144.47 50.13C145.15 50.41 145.36 50.53 145.65 50.91C146.53 54.49 148.21 56.48 150.23 56.39C152.48 56.49 155.61 54.33 159.68 50.04L159.47 49.78L158.98 49.99C155.08 51.94 152.61 52.83 151.73 52.84C150.58 52.75 149.86 51.98 149.28 50.15L149.28 49.94C149.30 49.31 150.01 48.83 151.25 48.31C157.54 44.65 160.66 39.53 160.64 32.90C160.72 27.83 158.56 23.38 154.42 19.78C151.43 17.14 147.41 15.78 142.53 15.84ZM135.37 20.48L135.67 19.98C136.94 18.75 138.81 18.33 141.04 18.25L143.49 18.35L143.42 21.53L135.40 21.56ZM144.52 18.43C145.38 18.49 146.21 18.78 146.73 18.89C147.62 19.29 148.01 19.78 148.02 20.44C147.91 21.13 147.46 21.58 146.31 21.48L144.47 21.58ZM135.49 23.95C135.36 23.07 135.86 22.68 136.66 22.57L143.50 22.66L143.51 26.27L136.49 26.25C135.96 26.22 135.61 25.88 135.41 25.12ZM144.52 22.68L150.52 22.53C151.51 22.72 152.11 23.38 152.76 25.07L152.65 25.11C152.70 25.76 152.26 26.13 151.50 26.26L144.53 26.29ZM129.27 32.63C129.70 28.01 130.46 25.68 131.55 25.66L131.89 25.75C132.61 25.67 132.96 26.18 132.96 27.17L132.95 38.81C133.00 39.91 132.56 40.43 131.75 40.46C131.46 40.42 131.16 40.33 130.81 40.07C129.82 37.98 129.36 35.53 129.37 32.73ZM135.50 28.51C135.38 27.79 135.81 27.48 136.58 27.29L143.55 27.41L143.45 31.32L137.24 31.35C136.04 31.35 135.41 30.88 135.44 30.05ZM144.49 27.35L153.00 27.31C153.86 27.42 154.21 28.23 154.51 30.12C154.38 30.84 153.81 31.33 152.53 31.29L144.48 31.35ZM135.34 33.35C135.37 32.83 135.81 32.53 136.47 32.28L143.51 32.37L143.43 34.14C141.97 33.88 140.56 33.78 139.22 33.78L139.16 33.77L139.20 33.96C140.03 33.84 141.21 34.28 142.38 34.94C144.52 36.68 145.66 39.03 145.62 41.83C145.41 43.92 145.41 45.28 145.36 45.67C145.38 46.99 144.81 47.68 143.68 47.64L140.23 47.74C137.34 47.45 135.66 46.63 135.49 45.55ZM144.41 32.27L153.37 32.28C154.11 32.42 154.41 32.68 154.71 33.42C154.66 34.87 154.41 36.28 154.06 37.77C152.72 41.63 151.61 43.53 150.72 43.53L150.38 43.45C149.68 43.29 149.36 43.03 149.33 42.49C149.45 40.46 149.06 38.73 148.50 37.66C147.43 36.04 146.16 35.03 144.43 34.49Z\"/></svg>"
}
 ```

# Login request

POST /login HTTP/1.1
Accept: application/json, text/plain, */*
Accept-Encoding: gzip, deflate, br
Accept-Language: es,en-US;q=0.9,en;q=0.8,it;q=0.7,ca;q=0.6,pt;q=0.5
Connection: keep-alive
Content-Length: 162
Content-Type: application/json;charset=UTF-8
Host: www.nauta.cu:5002
Origin: https://www.nauta.cu
Referer: https://www.nauta.cu/
Sec-Fetch-Dest: empty
Sec-Fetch-Mode: cors
Sec-Fetch-Site: same-site
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36
passwordApp: ApiKey <key>
sec-ch-ua: "Google Chrome";v="105", "Not)A;Brand";v="8", "Chromium";v="105"
sec-ch-ua-mobile: ?0
sec-ch-ua-platform: "Linux"
usernameApp: portal

## Post Payload

```json
{
  "username": "+53xxxxxxxx",
  "password": "passwd",
  "tipoCuenta": "USUARIO_PORTAL",
  "idRequest": "41d3a8ba8dee031b0f3ef93db6a1117b080fc5ed",
  "captchatext": "defzpz"
}
 ```

## Response

```json
{
  "resp": {
    "token": "<token>",
    "user": {
      "cliente": {
        "nombre": "<name>",
        "telefono": "",
        "email": "",
        "notificaciones_mail": "false",
        "notificaciones_movil": "false",
        "usuario_portal": "+53<phone_number>",
        "operaciones": {
          "Crear cuenta de navegación (@nauta.com.cu)": {
            "operacion": "Crear cuenta de navegación (@nauta.com.cu)",
            "url": "crearCuentaAcceso",
            "tipo": "ALTA",
            "comercioElectronico": "false",
            "metodo": "POST",
            "modo": "ASINCRONICO",
            "id": "create_cta_permanente",
            "alteraPerfilServicio": "NO",
            "parametros": {
              "Tipo de servicio": {
                "nombre": "Tipo de servicio",
                "parametro": "especificacionProductoId",
                "valor": "100",
                "tipo": "STRING",
                "orden": "4"
              },
              "Cuenta de navegación": {
                "nombre": "Cuenta de navegación",
                "parametro": "cuenta",
                "valor": "",
                "tipo": "STRING",
                "orden": "1"
              },
              "Contraseña": {
                "nombre": "Contraseña",
                "parametro": "password",
                "valor": "",
                "tipo": "NEWPASSWORD",
                "orden": "2"
              },
              "Cliente": {
                "nombre": "Cliente",
                "parametro": "clienteId",
                "valor": "<id_client>",
                "tipo": "STRING",
                "orden": "3"
              }
            }
          },
          "Crear cuenta de correo (@nauta.cu)": {
            "operacion": "Crear cuenta de correo (@nauta.cu)",
            "url": "crearCuentaCorreo",
            "tipo": "ALTA",
            "comercioElectronico": "false",
            "metodo": "POST",
            "modo": "ASINCRONICO",
            "id": "create_email",
            "alteraPerfilServicio": "NO",
            "parametros": {
              "Tipo de servicio": {
                "nombre": "Tipo de servicio",
                "parametro": "especificacionProductoId",
                "valor": "correo",
                "tipo": "STRING",
                "orden": "4"
              },
              "Cuenta de correo": {
                "nombre": "Cuenta de correo",
                "parametro": "cuenta",
                "valor": "",
                "tipo": "STRING",
                "orden": "1"
              },
              "Contraseña": {
                "nombre": "Contraseña",
                "parametro": "password",
                "valor": "",
                "tipo": "NEWPASSWORD",
                "orden": "2"
              },
              "Cliente": {
                "nombre": "Cliente",
                "parametro": "clienteId",
                "valor": "<clienteId>",
                "tipo": "STRING",
                "orden": "3"
              }
            }
          },
          "Pagos electrónicos en el portal": {
            "operacion": "Pagos electrónicos en el portal",
            "url": "queryPagosOnLine",
            "tipo": "CONSULTA",
            "comercioElectronico": "false",
            "metodo": "GET",
            "modo": "ASINCRONICO",
            "id": "query_pago_operaciones",
            "alteraPerfilServicio": "NO",
            "parametros": {
              "Cliente Id": {
                "nombre": "Cliente Id",
                "parametro": "clienteId",
                "valor": "<clienteId>",
                "tipo": "STRING",
                "orden": "1"
              },
              "Fecha": {
                "nombre": "Fecha",
                "parametro": "fecha",
                "valor": "",
                "tipo": "DATETIME",
                "orden": "2"
              },
              "pagina": {
                "nombre": "pagina",
                "parametro": "pagina",
                "valor": "0",
                "tipo": "NUMERIC",
                "orden": "3"
              },
              "Elementos por páginas": {
                "nombre": "Elementos por páginas",
                "parametro": "itemsPorPagina",
                "valor": "0",
                "tipo": "NUMERIC",
                "orden": "4"
              }
            }
          },
          "Operaciones realizadas en el portal": {
            "operacion": "Operaciones realizadas en el portal",
            "url": "queryOperations",
            "tipo": "CONSULTA",
            "comercioElectronico": "false",
            "metodo": "GET",
            "modo": "ASINCRONICO",
            "id": "query_operations",
            "alteraPerfilServicio": "NO",
            "parametros": {
              "Cliente Id": {
                "nombre": "Cliente Id",
                "parametro": "clienteId",
                "valor": "<clienteId>",
                "tipo": "STRING",
                "orden": "1"
              },
              "Fecha": {
                "nombre": "Fecha",
                "parametro": "fecha",
                "valor": "",
                "tipo": "DATETIME",
                "orden": "2"
              },
              "pagina": {
                "nombre": "pagina",
                "parametro": "pagina",
                "valor": "0",
                "tipo": "NUMERIC",
                "orden": "3"
              },
              "Elementos por páginas": {
                "nombre": "Elementos por páginas",
                "parametro": "itemsPorPagina",
                "valor": "0",
                "tipo": "NUMERIC",
                "orden": "4"
              }
            }
          }
        }
      },
      "Servicios": {
        "Correo Nauta": {
          "<mail>@nauta.cu": {
            "perfil": {
              "Fecha de venta": "01-10-2015",
              "Cuenta de correo": "<mail>@nauta.cu",
              "id": "<mail>"
            },
            "tipoProducto": "CORREO",
            "operaciones": {
              "Cambiar contraseña": {
                "operacion": "Cambiar contraseña",
                "url": "changeMailPassword",
                "tipo": "CAMBIO_PASSWORD",
                "comercioElectronico": "false",
                "metodo": "POST",
                "modo": "ASINCRONICO",
                "id": "change_passw_email",
                "alteraPerfilServicio": "NO",
                "parametros": {
                  "Cuenta de correo": {
                    "nombre": "Cuenta de correo",
                    "parametro": "cuenta",
                    "valor": "<mail>@nauta.cu",
                    "tipo": "STRING",
                    "orden": "1"
                  },
                  "Contraseña anterior": {
                    "nombre": "Contraseña anterior",
                    "parametro": "oldPassword",
                    "valor": "",
                    "tipo": "PASSWORD",
                    "orden": "2"
                  },
                  "Contraseña nueva": {
                    "nombre": "Contraseña nueva",
                    "parametro": "newPassword",
                    "valor": "",
                    "tipo": "NEWPASSWORD",
                    "orden": "3"
                  }
                }
              }
            }
          }
        },
        "Telefonía fija": {}
      },
      "servicios_actualizados": "false",
      "completado": "false",
      "fechaActualizacion": "2022-09-11 15:14:16"
    },
    "resultado": "ok"
  }
}
```

# User Data

URL: https://www.nauta.cu:5002/users

Accept: application/json, text/plain, */*
Accept-Encoding: gzip, deflate, br
Accept-Language: es,en-US;q=0.9,en;q=0.8,it;q=0.7,ca;q=0.6,pt;q=0.5
Authorization: Bearer
eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiIrNTM1OTY0MjEwMSIsImlhdCI6MTY2MzE1MjQyNiwiaXNzIjoiZ2VzTmF1dGFTZXJ2aWNlcyIsImV4cCI6MTY2MzE1NjAyNiwidXNlcl9pbmZvIjoie1wibGxwZGNcIjpcImFlNGQ2MmVlLWI4NTctNDk2OC1hZTlkLWJjNzUwMWZlNTBmYlwiLFwiaWRlbnRpZmljYWNpb25cIjpcIjg4MTIxMDAwMjY2XCIsXCJ0aXBvSWRlbnRpZmljYWNpb25cIjpcIjFcIixcInBlcnNvbmFfaWRcIjpcImFlNGQ2MmVlLWI4NTctNDk2OC1hZTlkLWJjNzUwMWZlNTBmYlwiLFwidXN1YXJpb19wb3J0YWxcIjpcIis1MzU5NjQyMTAxXCIsXCJub21icmVcIjpcInJ1YmlzZWwgcHJpZXRvIGR1cGV5cm9uXCIsXCJjb3JyZW9cIjpcIlwiLFwidGVsZWZvbm9cIjpcIlwifSJ9.kh3apHM7H7SOPW08pFYD86K_EuD4kGeJiGN3dxVP5wzbsjESjiKu2Hhp-qRfOWuD
Connection: keep-alive
Content-Length: 67
Content-Type: application/json;charset=UTF-8
Host: www.nauta.cu:5002
Origin: https://www.nauta.cu
passwordApp: ApiKey CvSf/wpT0/ynAqWYmBHndsKb6onae5422T7kSjOG3cYUZtuiWSFGCbYmYxJLfAIsi5cfKc1qsNwQSvBOT5fTjQ==
Referer: https://www.nauta.cu/
sec-ch-ua: "Google Chrome";v="105", "Not)A;Brand";v="8", "Chromium";v="105"
sec-ch-ua-mobile: ?0
sec-ch-ua-platform: "Linux"
Sec-Fetch-Dest: empty
Sec-Fetch-Mode: cors
Sec-Fetch-Site: same-site
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36
usernameApp: portal

## Payload

```json
{
  "email": "+5359642101",
  "ultimaActualizacion": "2022-09-14 06:47:08"
}
```

## Response

```json
{
  "resp": {
    "user": {
      "cliente": {
        "nombre": "rubisel prieto dupeyron",
        "telefono": "",
        "email": "",
        "notificaciones_mail": "false",
        "notificaciones_movil": "false",
        "usuario_portal": "+5359642101",
        "operaciones": {
          "Crear cuenta de navegación (@nauta.com.cu)": {
            "operacion": "Crear cuenta de navegación (@nauta.com.cu)",
            "url": "crearCuentaAcceso",
            "tipo": "ALTA",
            "comercioElectronico": "false",
            "metodo": "POST",
            "modo": "ASINCRONICO",
            "id": "create_cta_permanente",
            "alteraPerfilServicio": "NO",
            "parametros": {
              "Tipo de servicio": {
                "nombre": "Tipo de servicio",
                "parametro": "especificacionProductoId",
                "valor": "100",
                "tipo": "STRING",
                "orden": "4"
              },
              "Cuenta de navegación": {
                "nombre": "Cuenta de navegación",
                "parametro": "cuenta",
                "valor": "",
                "tipo": "STRING",
                "orden": "1"
              },
              "Contraseña": {
                "nombre": "Contraseña",
                "parametro": "password",
                "valor": "",
                "tipo": "NEWPASSWORD",
                "orden": "2"
              },
              "Cliente": {
                "nombre": "Cliente",
                "parametro": "clienteId",
                "valor": "ae4d62ee-b857-4968-ae9d-bc7501fe50fb",
                "tipo": "STRING",
                "orden": "3"
              }
            }
          },
          "Crear cuenta de correo (@nauta.cu)": {
            "operacion": "Crear cuenta de correo (@nauta.cu)",
            "url": "crearCuentaCorreo",
            "tipo": "ALTA",
            "comercioElectronico": "false",
            "metodo": "POST",
            "modo": "ASINCRONICO",
            "id": "create_email",
            "alteraPerfilServicio": "NO",
            "parametros": {
              "Tipo de servicio": {
                "nombre": "Tipo de servicio",
                "parametro": "especificacionProductoId",
                "valor": "correo",
                "tipo": "STRING",
                "orden": "4"
              },
              "Cuenta de correo": {
                "nombre": "Cuenta de correo",
                "parametro": "cuenta",
                "valor": "",
                "tipo": "STRING",
                "orden": "1"
              },
              "Contraseña": {
                "nombre": "Contraseña",
                "parametro": "password",
                "valor": "",
                "tipo": "NEWPASSWORD",
                "orden": "2"
              },
              "Cliente": {
                "nombre": "Cliente",
                "parametro": "clienteId",
                "valor": "ae4d62ee-b857-4968-ae9d-bc7501fe50fb",
                "tipo": "STRING",
                "orden": "3"
              }
            }
          },
          "Pagos electrónicos en el portal": {
            "operacion": "Pagos electrónicos en el portal",
            "url": "queryPagosOnLine",
            "tipo": "CONSULTA",
            "comercioElectronico": "false",
            "metodo": "GET",
            "modo": "ASINCRONICO",
            "id": "query_pago_operaciones",
            "alteraPerfilServicio": "NO",
            "parametros": {
              "Cliente Id": {
                "nombre": "Cliente Id",
                "parametro": "clienteId",
                "valor": "ae4d62ee-b857-4968-ae9d-bc7501fe50fb",
                "tipo": "STRING",
                "orden": "1"
              },
              "Fecha": {
                "nombre": "Fecha",
                "parametro": "fecha",
                "valor": "",
                "tipo": "DATETIME",
                "orden": "2"
              },
              "pagina": {
                "nombre": "pagina",
                "parametro": "pagina",
                "valor": "0",
                "tipo": "NUMERIC",
                "orden": "3"
              },
              "Elementos por páginas": {
                "nombre": "Elementos por páginas",
                "parametro": "itemsPorPagina",
                "valor": "0",
                "tipo": "NUMERIC",
                "orden": "4"
              }
            }
          },
          "Operaciones realizadas en el portal": {
            "operacion": "Operaciones realizadas en el portal",
            "url": "queryOperations",
            "tipo": "CONSULTA",
            "comercioElectronico": "false",
            "metodo": "GET",
            "modo": "ASINCRONICO",
            "id": "query_operations",
            "alteraPerfilServicio": "NO",
            "parametros": {
              "Cliente Id": {
                "nombre": "Cliente Id",
                "parametro": "clienteId",
                "valor": "ae4d62ee-b857-4968-ae9d-bc7501fe50fb",
                "tipo": "STRING",
                "orden": "1"
              },
              "Fecha": {
                "nombre": "Fecha",
                "parametro": "fecha",
                "valor": "",
                "tipo": "DATETIME",
                "orden": "2"
              },
              "pagina": {
                "nombre": "pagina",
                "parametro": "pagina",
                "valor": "0",
                "tipo": "NUMERIC",
                "orden": "3"
              },
              "Elementos por páginas": {
                "nombre": "Elementos por páginas",
                "parametro": "itemsPorPagina",
                "valor": "0",
                "tipo": "NUMERIC",
                "orden": "4"
              }
            }
          }
        }
      },
      "Servicios": {
        "Correo Nauta": {
          "rise@nauta.cu": {
            "perfil": {
              "Fecha de venta": "01-10-2015",
              "Cuenta de correo": "rise@nauta.cu",
              "id": "15065114965"
            },
            "tipoProducto": "CORREO",
            "operaciones": {
              "Cambiar contraseña": {
                "operacion": "Cambiar contraseña",
                "url": "changeMailPassword",
                "tipo": "CAMBIO_PASSWORD",
                "comercioElectronico": "false",
                "metodo": "POST",
                "modo": "ASINCRONICO",
                "id": "change_passw_email",
                "alteraPerfilServicio": "NO",
                "parametros": {
                  "Cuenta de correo": {
                    "nombre": "Cuenta de correo",
                    "parametro": "cuenta",
                    "valor": "rise@nauta.cu",
                    "tipo": "STRING",
                    "orden": "1"
                  },
                  "Contraseña anterior": {
                    "nombre": "Contraseña anterior",
                    "parametro": "oldPassword",
                    "valor": "",
                    "tipo": "PASSWORD",
                    "orden": "2"
                  },
                  "Contraseña nueva": {
                    "nombre": "Contraseña nueva",
                    "parametro": "newPassword",
                    "valor": "",
                    "tipo": "NEWPASSWORD",
                    "orden": "3"
                  }
                }
              }
            }
          }
        },
        "Servicios móviles": {
          "5359642101": {
            "perfil": {
              "id": "5359642101",
              "Número de Teléfono": "5359642101",
              "Estado": "Activo",
              "Saldo principal": "109.84",
              "Fecha de Suspensión": "12/04/2023",
              "Fecha de Eliminación": "12/05/2023",
              "Internet": "HABILITADO",
              "4G": "HABILITADO",
              "Adelanta Saldo": "0",
              "Tarifa por consumo": "NO DEFINIDO",
              "Listas": {
                "Planes": {
                  "3": {
                    "tipo": "MINUTOS",
                    "Fecha inicio": "",
                    "Fecha fin": "28/09/2022",
                    "Datos": "03:31:35"
                  },
                  "4": {
                    "tipo": "SMS",
                    "Fecha inicio": "",
                    "Fecha fin": "28/09/2022",
                    "Datos": "465"
                  },
                  "7": {
                    "tipo": "DATOS",
                    "Fecha inicio": "24/03/2020",
                    "Fecha fin": "12/10/2022",
                    "Datos": "4GB"
                  },
                  "20": {
                    "tipo": "DATOS LTE",
                    "Fecha inicio": "24/03/2020",
                    "Fecha fin": "12/10/2022",
                    "Datos": "8.634GB"
                  }
                },
                "Bonos": {
                  "16": {
                    "tipo": "DATOS NACIONALES",
                    "Fecha inicio": "",
                    "Fecha fin": "04/10/2022",
                    "Datos": "291.118MB"
                  },
                  "23": {
                    "tipo": "DATOS LTE",
                    "Fecha inicio": "24/03/2020",
                    "Fecha fin": "12/10/2022",
                    "Datos": "0"
                  }
                }
              }
            },
            "operaciones": {}
          },
          "5358116834": {
            "perfil": {
              "id": "5358116834",
              "Número de Teléfono": "5358116834",
              "Estado": "Activo",
              "Saldo principal": "288.6",
              "Fecha de Suspensión": "28/03/2023",
              "Fecha de Eliminación": "28/04/2023",
              "Internet": "HABILITADO",
              "4G": "HABILITADO",
              "Adelanta Saldo": "0",
              "Tarifa por consumo": "ACTIVADO",
              "Listas": {
                "Planes": {
                  "3": {
                    "tipo": "MINUTOS",
                    "Fecha inicio": "",
                    "Fecha fin": "29/09/2022",
                    "Datos": "01:40:05"
                  },
                  "4": {
                    "tipo": "SMS",
                    "Fecha inicio": "",
                    "Fecha fin": "29/09/2022",
                    "Datos": "252"
                  },
                  "7": {
                    "tipo": "DATOS",
                    "Fecha inicio": "18/03/2022",
                    "Fecha fin": "29/09/2022",
                    "Datos": "3.015GB"
                  },
                  "20": {
                    "tipo": "DATOS LTE",
                    "Fecha inicio": "18/03/2022",
                    "Fecha fin": "29/09/2022",
                    "Datos": "0B"
                  }
                },
                "Bonos": {
                  "16": {
                    "tipo": "DATOS NACIONALES",
                    "Fecha inicio": "",
                    "Fecha fin": "28/09/2022",
                    "Datos": "75.134MB"
                  },
                  "23": {
                    "tipo": "DATOS LTE",
                    "Fecha inicio": "18/03/2022",
                    "Fecha fin": "29/09/2022",
                    "Datos": "0"
                  }
                }
              }
            },
            "operaciones": {}
          }
        },
        "Telefonía fija": {}
      },
      "servicios_actualizados": "true",
      "completado": "true",
      "fechaActualizacion": "2022-09-14 06:47:19"
    },
    "resultado": "ok"
  }
}
```