import 'package:flutter/material.dart';
import '../models/data_nauta.dart';
import '../models/data_user.dart';
import '../services/ApiServices.dart';

import 'widgets/dialog.dart';
import 'widgets/w_mobile_services.dart';
import 'widgets/w_mobile_service.dart';

class Nauta extends StatefulWidget {
  final DataNauta? dataNauta;
  final String? username;

  const Nauta({super.key, required this.dataNauta, required this.username});

  @override
  State<StatefulWidget> createState() {
    return _NautaState();
  }
}

class _NautaState extends State<Nauta> {
  late bool loading = true;
  late DataUser dataUser;
  late WMobileServices planesWidgets;

  @override
  void initState() {
    super.initState();
    _getData();
  }

  void _getData() async {
    try {
      dataUser = await ApiService().users(widget.username!, widget.dataNauta!.token);
      loading = false;
      Future.delayed(const Duration(seconds: 1)).then((value) => setState(() {}));
      planesWidgets = WMobileServices(mobileServices: dataUser.services!.mobileService.values.toList());
    } on Exception catch (e) {
      showAlertDialog(context,'Error', e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Datos"),
        ),
        body: loading
            ? const Center(
          child: CircularProgressIndicator(),
        )
            : planesWidgets
    );
  }
}
