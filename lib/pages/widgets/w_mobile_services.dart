import 'package:flutter/material.dart';

import '../../models/data_user.dart';

class WMobileServices extends StatefulWidget {
  const WMobileServices({super.key, required this.mobileServices});

  final List<MobileService> mobileServices;

  @override
  _WMobileServicesState createState() => _WMobileServicesState();
}

class _WMobileServicesState extends State<WMobileServices> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      child: ListView.builder(
        itemCount: widget.mobileServices.length,
        itemBuilder: (BuildContext context, int index) {
          return ExpansionPanelList(
            animationDuration: const Duration(milliseconds: 1000),
            dividerColor: Colors.red,
            elevation: 1,
            children: [
              ExpansionPanel(
                body: Container(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Saldo principal: ${widget.mobileServices[index].principalBalance}"),
                      Text("Fecha de Suspensión: ${widget.mobileServices[index].suspensionDate}"),
                      Text("Internet: ${widget.mobileServices[index].internet}"),
                      Text("4G: ${widget.mobileServices[index].s4G}"),
                      Text("Adelanta Saldo: ${widget.mobileServices[index].advanceBalance}"),
                      Text("Tarifa por consumo: ${widget.mobileServices[index].rateConsumption}"),
                    ],
                  ),
                ),
                headerBuilder: (BuildContext context, bool isExpanded) {
                  return Container(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      "Número de Teléfono: ${widget.mobileServices[index].number}",
                    ),
                  );
                },
                isExpanded: true, // todo
              )
            ],
            expansionCallback: (int item, bool status) {
              setState(() {
                // Todo
                // itemData[index].expanded = !itemData[index].expanded;
              });
            },
          );
        },
      ),
    );
  }
}
