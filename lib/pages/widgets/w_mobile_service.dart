import 'package:flutter/material.dart';

import '../../models/data_user.dart';

class WMobileService extends StatelessWidget {
  const WMobileService({Key? key, required this.mobileService,
  }) : super(key: key);

  final MobileService mobileService;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(4, 4, 4, 12),
      child: Align(
        alignment: Alignment.topCenter,
        child: Text(mobileService.number, style: Theme.of(context).textTheme.titleMedium),
      ),
    );
  }
}