import 'package:flutter/material.dart';

showAlertDialog(BuildContext context, String title, String content ) {
  AlertDialog alert = AlertDialog(
    title: Text(title),
    content: Text(content),
    actions: [
      TextButton(
        child: const Text("OK"),
        onPressed: () {

        },
      )
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}