import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../models/user.dart';

class NewUser extends StatefulWidget {
  const NewUser({super.key});

  @override
  NewUserState createState() {
    return NewUserState();
  }
}

class NewUserState extends State<NewUser> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    TextEditingController nameController = TextEditingController();
    TextEditingController userNameController = TextEditingController();
    TextEditingController passwordController = TextEditingController();

    void _submit() {
      final isValid = _formKey.currentState!.validate();
      if (!isValid) {
        return;
      }
      _formKey.currentState!.save();
      final user = User(
        id: userNameController.text,
        name: nameController.text,
        username: userNameController.text,
        password: passwordController.text,
      );
      user.save();
      Navigator.pop(context);
    }

    return Scaffold(
        appBar: AppBar(title: const Text('Add user')),
        body: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextFormField(
                controller: nameController,
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Name can not be empty!';
                  }
                  return null;
                },
                decoration: const InputDecoration(
                  icon: Icon(Icons.person),
                  hintText: 'Enter your name',
                  labelText: 'Name',
                ),
              ),
              TextFormField(
                controller: userNameController,
                keyboardType: TextInputType.number,
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Number can not be empty!';
                  }
                  return null;
                },
                inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                decoration: const InputDecoration(
                  icon: Icon(Icons.phone),
                  hintText: 'Enter a phone number',
                  labelText: 'Phone',
                ),
              ),
              TextFormField(
                controller: passwordController,
                obscureText: true,
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Enter a valid password!';
                  }
                  return null;
                },
                decoration: const InputDecoration(
                  icon: Icon(Icons.calendar_today),
                  hintText: 'Enter password',
                  labelText: 'Password',
                ),
              ),
              Center(
                child: Container(
                    child: ElevatedButton(
                  child: Text('Submit'),
                  onPressed: () => _submit(),
                )),
              ),
            ],
          ),
        ));
  }
}
