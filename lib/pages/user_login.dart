import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';
import 'package:fnauta/pages/nauta.dart';
import 'package:fnauta/pages/widgets/dialog.dart';
import '../services/ApiServices.dart';
import '../models/captcha.dart';
import '../models/user.dart';
import '../models/data_nauta.dart';

class UserLogin extends StatefulWidget {
  final User user;

  const UserLogin({super.key, required this.user});

  @override
  State<StatefulWidget> createState() {
    return _UserLoginState();
  }
}

class _UserLoginState extends State<UserLogin> {
  late Captcha? captcha = null;
  late DataNauta dataNauta;
  late bool loading = true;
  String errorMsg = '';
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _getData();
  }

  void _getData() async {
    captcha = (await ApiService().getCaptcha())!;
    loading = false;
    Future.delayed(const Duration(seconds: 1)).then((value) => setState(() {}));
  }

  Future<void> _login(captchaText) async {
    try {
      dataNauta = await ApiService().login(widget.user.username,
          widget.user.password, captcha!.text, captchaText);
      Future.delayed(const Duration(seconds: 1))
          .then((value) => setState(() {}));
    } catch (e) {
      errorMsg = "Error al iniciar secion: $e";
      showAlertDialog(context, "Error al iniciar secion", e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    TextEditingController captchaController = TextEditingController();

    return Scaffold(
        appBar: AppBar(
          title: Text("Login in nauta.cu: ${widget.user.name}"),
        ),
        body: Form(
            key: _formKey,
            child: loading
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : Column(
                    children: [
                      SvgPicture.string(captcha!.data),
                      TextFormField(
                        controller: captchaController,
                        decoration: InputDecoration(
                            hintText: 'Write captcha in image',
                            labelText: 'Captcha',
                            errorText: errorMsg),
                      ),
                      Container(
                          padding:
                              const EdgeInsets.only(left: 150.0, top: 40.0),
                          child: ElevatedButton(
                            child: const Text('Submit'),
                            onPressed: () async {
                              loading = true;
                              setState(() {
                                errorMsg = ''; // clear any existing errors
                              });

                              await _login(captchaController.text);
                              loading = false;

                              if (errorMsg.isEmpty && dataNauta.isLogin()) {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Nauta(
                                          dataNauta: dataNauta,
                                          username: widget.user.username)),
                                );
                              } else {
                                setState(() {
                                  errorMsg =
                                      'Ocurio un error'; // clear any existing errors
                                });
                              }
                            },
                          ))
                    ],
                  )));
  }
}
