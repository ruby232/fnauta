import 'package:flutter/cupertino.dart';

class DataUser {
  Client? client;
  Services? services;
  String? updatedServices;
  String? complete;
  String? dateUpdate;

  DataUser(
      {this.client,
      this.services,
      this.updatedServices,
      this.complete,
      this.dateUpdate});

  DataUser.fromJson(Map<String, dynamic> json) {
    client = json['cliente'] != null ? Client.fromJson(json['cliente']) : null;
    services =
        json['Servicios'] != null ? Services.fromJson(json['Servicios']) : null;
    updatedServices = json['servicios_actualizados'];
    complete = json['completado'];
    dateUpdate = json['fechaActualizacion'];
  }
}

class Client {
  String? name;
  String? phone;
  String? email;
  bool? emailNotifications;
  bool? mobileNotifications;
  String? portalUser;

  Client(
      {this.name,
      this.phone,
      this.email,
      this.emailNotifications,
      this.mobileNotifications,
      this.portalUser});

  Client.fromJson(Map<String, dynamic> json) {
    name = json['nombre'];
    phone = json['telefono'];
    email = json['email'];
    emailNotifications = json['notificaciones_mail'] == 'true';
    mobileNotifications = json['notificaciones_movil'] == 'true';
    portalUser = json['usuario_portal'];
  }
}

class Services {
  Map<String, MobileService> mobileService = {};

  MailNauta? mailNauta;
  FixedTelephony? fixedTelephony;

  Services({this.mailNauta, this.fixedTelephony});

  Services.fromJson(Map<String, dynamic> json) {
    if (json['Servicios móviles'] != null) {
      json['Servicios móviles'].forEach((k,v) {
        mobileService[k] = MobileService.fromJson(v["perfil"]);
      });
    }

    mailNauta = json['Correo Nauta'] != null
        ? MailNauta.fromJson(json['Correo Nauta'])
        : null;
    fixedTelephony = json['Telefonía fija'] != null
        ? FixedTelephony.fromJson(json['Telefonía fija'])
        : null;
  }
}

//todo
class FixedTelephony {
  FixedTelephony.fromJson(Map<String, dynamic> json) {}
}

//todo
class MailNauta {
  MailNauta.fromJson(Map<String, dynamic> json) {}
}

class MobileService {
  String? id;
  late String number;
  String? status;
  String? principalBalance;
  String? suspensionDate;
  String? removalDate;
  String? internet;
  String? s4G;
  String? advanceBalance;
  String? rateConsumption;
  Map<String, Plan>? plans = {};

  MobileService.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    number = json['Número de Teléfono'];
    status = json['Estado'];
    principalBalance = json['Saldo principal'];
    suspensionDate = json['Fecha de Suspensión'];
    removalDate = json['Fecha de Eliminación'];
    internet = json['Internet'];
    s4G = json['4G'];
    advanceBalance = json['Adelanta Saldo'];
    rateConsumption = json['Tarifa por consumo'];

    if (json['Listas'] != null) {
      if (json['Listas']['Planes'] != null) {
        json['Listas']['Planes'].forEach((k, v) {
          plans![k] = Plan.fromJson(v, PlanKind.plan);
        });
      }
      if (json['Listas']['Bonos'] != null) {
        json['Listas']['Bonos'].forEach((k, v) {
          plans![k] = Plan.fromJson(v, PlanKind.bonus);
        });
      }
    }
  }
}

enum PlanKind { plan, bonus }

class Plan {
  PlanKind kind = PlanKind.plan;
  String? type;
  String? startDate;
  String? endDate;
  String? data;

  Plan({this.type, this.startDate, this.endDate, this.data});

  Plan.fromJson(Map<String, dynamic> json, PlanKind pkind) {
    type = json['tipo'];
    startDate = json['Fecha inicio'];
    endDate = json['Fecha fin'];
    data = json['Datos'];
    kind = pkind;
  }
}
