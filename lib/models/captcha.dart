class Captcha {
  Captcha({
    required this.text,
    required this.data,
  });

  String text;
  String data;

  factory Captcha.fromJson(Map<String, dynamic> json){
    String data = json["data"];
    data = data.replaceAll('100%', '100');
    return Captcha(
        text: json["text"],
        data: data
    );
  }
}