class DataNauta {
  DataNauta({
    required this.token,
    required this.result,
    required this.user,
  });

  String token;
  String result;
  Map user;

  factory DataNauta.fromJson(Map<String, dynamic> json) => DataNauta(
        token: json["resp"]["token"],
        result: json["resp"]["resultado"],
        user: json["resp"]["user"],
      );

  bool isLogin() {
    return result == 'ok';
  }
}
