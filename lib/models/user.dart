import 'package:localstore/localstore.dart';

// Data Model
class User {
  final String id;
  String name;
  String password;
  String username;

  User({
    required this.id,
    required this.name,
    required this.username,
    required this.password,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'password': password,
      'username': username,
    };
  }

  factory
  User.fromMap(Map<String, dynamic> map) {
    return User(
      id: map['id'],
      name: map['name'],
      password: map['password'],
      username: map['username'],
    );
  }
}

extension ExtUser on User {
  Future save() async {
    final _db = Localstore.instance;
    return _db.collection('users').doc(id).set(toMap());
  }

  Future delete() async {
    final _db = Localstore.instance;
    return _db.collection('users').doc(id).delete();
  }
}