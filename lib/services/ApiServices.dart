import 'dart:developer';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import '../models/captcha.dart';
import '../models/data_nauta.dart';
import '../models/data_user.dart';

class ApiService {
  String baseUrl = 'https://www.nauta.cu:5002';

  Future<Map<String, dynamic>> _post(Uri url, Map payload, {Map<String, String>? headers, int? delay}) async {
    if (delay != null) {
      // This code is necessary because api no response all data is executed immediately
      await Future.delayed(Duration(seconds: delay));
    }

    Response response = await http.post(url, body: payload, headers: headers);
    if (response.statusCode != 200) {
      throw RequestError(response.statusCode, response.body.toString());
    }

    var jsonData = json.decode(response.body);
    if (jsonData["resp"] == null) {
      throw ResponseError("Llave resp no encontrada en la respuesta.");
    }

    if (jsonData["resp"]["resultado"] != 'ok') {
      throw ResponseError(jsonData["resp"]["resultado"]);
    }

    return jsonData;
  }

  Future<Captcha?> getCaptcha() async {
    try {
      var url = Uri.parse('$baseUrl/captcha/captcha');
      var response = await http.get(url);
      if (response.statusCode == 200) {
        return Captcha.fromJson(json.decode(response.body));
      }
    } catch (e) {
      log(e.toString());
    }
    return null;
  }

  Future<DataNauta> login(String username, String password, String idRequest,
      String captcha) async {
    var url = Uri.parse('$baseUrl/login');
    var payload = {
      "username": '+53$username',
      "password": password,
      "tipoCuenta": "USUARIO_PORTAL",
      "idRequest": idRequest,
      "captchatext": captcha
    };
    try {
      Map<String, dynamic> data = await _post(url, payload);
      return DataNauta.fromJson(data);
    } on ResponseError catch (e) {
      if(e.toString() == 'errorCaptcha'){
        throw ErrorCaptcha();
      }
      rethrow;
    } catch (e) {
      rethrow;
    }
  }

  Future<DataUser> users(String username, String token) async {
    var url = Uri.parse('$baseUrl/users');
    DateTime today = DateTime.now();

    var payload = {
      "email": '+53$username',
      "ultimaActualizacion":
          "${today.year}-${today.month}-${today.day} ${today.hour}:${today.minute}:${today.second}"
    };
    var headers = {'Authorization': 'Bearer $token'};
    // This code is necessary because api no response all data is executed immediately
    await Future.delayed(const Duration(seconds: 10));

    Response response = await http.post(url, body: payload, headers: headers);
    if (response.statusCode == 200) {
      var jsonData = json.decode(response.body);
      if (jsonData["resp"] != null && jsonData["resp"]["resultado"] == 'ok') {
        return DataUser.fromJson(jsonData["resp"]["user"]);
      }
      throw Exception(
          'Api response with result: ${jsonData["resp"]["resultado"]}');
    }
    throw Exception('Api response with status code: ${response.statusCode}');
  }
}

class ResponseError implements Exception {
  final String message;

  ResponseError(this.message);

  @override
  String toString() {
    return message;
  }
}

class RequestError implements Exception {
  final int statusCode;
  final String message;

  RequestError(this.statusCode, this.message);

  @override
  String toString() {
    return "Status Code: $statusCode, $message";
  }
}

class ErrorCaptcha implements Exception {
  @override
  String toString() {
    return "Código captcha incorrecto";
  }
}
